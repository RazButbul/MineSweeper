import React from "react";
import "./Board.css";
import Cell from "./Cell";

const Board = () => {
  const numRows = 9;
  const numCols = 9;

  const createBoard = () => {
    let board = [];
    for (let i = 0; i < numRows; i++) {
      for (let j = 0; j < numCols; j++) {
        board.push(<Cell key={`${i}-${j}`} className="Cell" />);
      }
    }
    return board;
  };

  return <div className="Board">{createBoard()}</div>;
};

export default Board;
