// App.js
import React from "react";
import Board from "./Components/Board";
import "./App.css";

function App() {
  return (
    <div>
      <h1>Minesweeper</h1>
      <Board />
    </div>
  );
}

export default App;
